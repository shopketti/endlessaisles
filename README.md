# EndlessAisles

ShopKetti's custom Endless Aisles gem

API Docs: http://docs.endlessaisles.io

Created by Earl Balai Jr - CTO/Co-Founder of ShopKetti

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'EndlessAisles', git: 'git@bitbucket.org:shopketti/endlessaisles.git'
```

And then execute:

    $ bundle
