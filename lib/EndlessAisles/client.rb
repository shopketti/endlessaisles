require "EndlessAisles/client/orders"
require "EndlessAisles/client/products"
require "EndlessAisles/client/inventory"

module EndlessAisles
  class Client

    include HTTParty
    include EndlessAisles::Client::Orders
    include EndlessAisles::Client::Products
    include EndlessAisles::Client::Inventory

    test_base_uri = "https://app-qa.endlessaisles.io/"
    base_uri = "https://app.endlessaisles.io/"

    format :json

    def initialize(access_token = nil, testing = false)
      access_token ||= ENV["EA_ACCESS_TOKEN"]
      testing ||= ENV["EA_TEST_MODE"]

      if testing
        base_uri = test_base_uri
      end

      self.class.default_options.merge!(headers: { 'X-EA-REQUEST-TOKEN' => access_token })
    end

  end
end