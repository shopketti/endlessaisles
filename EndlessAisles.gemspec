lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "EndlessAisles/version"

Gem::Specification.new do |spec|
  spec.name          = "EndlessAisles"
  spec.version       = EndlessAisles::VERSION
  spec.authors       = ["Earl Balai Jr"]
  spec.email         = ["earl@kiwi.io"]

  spec.summary       = "This was created to interact with the Endless Aisles API for ShopKetti."
  spec.description   = "A gem that interacts with the Endless Aisles API."
  spec.homepage      = "http://dev.shopketti.com/gems/endlessaisles"
  spec.license       = "MIT"

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  #spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  #spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 10.0"
  # Dependencies
  spec.add_dependency "httparty"
end
